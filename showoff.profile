<?php
/**
 * @file
 * Drupal Showoff profile functions.
 */

/**
 * Implements hook_install_tasks_alter().
 */
function showoff_install_tasks_alter(&$tasks, $install_state) {
  global $install_state;
  // Skip profile selection step.
  $tasks['install_select_profile']['display'] = FALSE;
  $tasks['install_select_profile']['run'] = INSTALL_TASK_SKIP;
  // Skip language selection install step and default language to English.
  $tasks['install_select_locale']['display'] = FALSE;
  $tasks['install_select_locale']['run'] = INSTALL_TASK_SKIP;
  $install_state['parameters']['locale'] = 'en';
  $install_state['parameters']['locale'] = 'showoff';
  // Override "install_finished" task to redirect people to home page.
  $tasks['install_finished']['function'] = 'showoff_install_finished';
}

/**
 *
 */
function showoff_install_tasks(&$install_state) {
  return array();
}

/**
 * Override of install_finished() without the useless text.
 */
function showoff_install_finished(&$install_state) {
  // Flush all caches to ensure that any full bootstraps during the installer
  // do not leave stale cached data, and that any content types or other items
  // registered by the installation profile are registered correctly.
  drupal_flush_all_caches();

  // Remember the profile which was used.
  variable_set('install_profile', drupal_get_profile());

  // Installation profiles are always loaded last
  db_update('system')
    ->fields(array('weight' => 1000))
    ->condition('type', 'module')
    ->condition('name', drupal_get_profile())
    ->execute();

  // Cache a fully-built schema.
  drupal_get_schema(NULL, TRUE);

  // Run cron to populate update status tables (if available) so that users
  // will be warned if they've installed an out of date Drupal version.
  // Will also trigger indexing of profile-supplied content or feeds.
  drupal_cron_run();
  // END copy/paste from install_finished().

  if (isset($messages['error'])) {
    $output = '<p>' . (isset($messages['error']) ? st('Review the messages above before visiting <a href="@url">your new site</a>.', array('@url' => url(''))) : st('<a href="@url">Visit your new site</a>.', array('@url' => url('')))) . '</p>';
    return $output;
  }
  else {
    // If we don't install drupal using Drush, redirect the user to the front
    // page.
    if (!drupal_is_cli()) {
      drupal_goto('');
    }
  }
}
