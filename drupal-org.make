api = 2
core = 7.x

; Showoff modules
projects[showoff_feeds][type] = "module"
projects[showoff_feeds][subdir] = "contrib"

projects[showoff_fields][type] = "module"
projects[showoff_fields][subdir] = "contrib"

projects[showoff_image][type] = "module"
projects[showoff_image][subdir] = "contrib"

projects[showoff_misc][type] = "module"
projects[showoff_misc][subdir] = "contrib"

projects[showoff_unit][type] = "module"
projects[showoff_unit][subdir] = "contrib"

projects[showoff_video][type] = "module"
projects[showoff_video][subdir] = "contrib"

; Contrib modules
projects[ctools][type] = "module"
projects[ctools][subdir] = "contrib"

projects[date][type] = "module"
projects[date][subdir] = "contrib"

projects[devel][type] = "module"
projects[devel][subdir] = "contrib"

projects[entity][type] = "module"
projects[entity][subdir] = "contrib"

projects[features][type] = "module"
projects[features][subdir] = "contrib"

projects[module_filter][type] = "module"
projects[module_filter][subdir] = "contrib"

projects[pathauto][type] = "module"
projects[pathauto][subdir] = "contrib"

projects[strongarm][type] = "module"
projects[strongarm][subdir] = "contrib"

projects[token][type] = "module"
projects[token][subdir] = "contrib"

projects[views][type] = "module"
projects[views][subdir] = "contrib"

projects[views_bulk_operations][type] = "module"
projects[views_bulk_operations][subdir] = "contrib"

; Theme

projects[titan][type] = "theme"
projects[titan][subdir] = "contrib"
