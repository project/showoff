api = 2
core = 7.x

includes[] = "drupal-org-core.make"

projects[showoff][type] = "profile"
projects[showoff][download][type] = "git"
projects[showoff][download][url] = "http://git.drupal.org/project/showoff.git"
projects[showoff][download][branch] = "7.x-1.x"
